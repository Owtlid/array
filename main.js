// students
let Jean = [];
let Claude = [];
let Van = [];
let Damme = [];

// sCredits
let credits = [];
credits["javascript"] = 4;
credits["react"] = 7;
credits["python"] = 6;
credits["java"] = 3;
credits["sum"] =
  credits["javascript"] +
  credits["react"] +
  credits["python"] +
  credits["java"];
console.log("Course Sredits: ", credits);


//  Jean Reno
Jean["FirstName"] = "Jean";
Jean["LastName"] = "Reno";
Jean["Age"] = 26;
Jean["Scores"] = [];
Jean["Scores"]["javascript"] = 62;
Jean["Scores"]["react"] = 57;
Jean["Scores"]["python"] = 88;
Jean["Scores"]["java"] = 90;
Jean["Sumofpoints"] =
  Jean["Scores"]["javascript"] +
  Jean["Scores"]["react"] +
  Jean["Scores"]["python"] +
  Jean["Scores"]["java"];
Jean["Averageofpoints"] = Jean["Sumofpoints"] / 4;
Jean["GPA"] =
  (credits["javascript"] * 1 +
    credits["react"] * 0.5 +
    credits["python"] * 3 +
    credits["java"] * 3) /
  credits["sum"];

// Claude Monet
Claude["FirstName"] = "Claude";
Claude["LastName"] = "Monet";
Claude["Age"] = 19;
Claude["Scores"] = [];
Claude["Scores"]["javascript"] = 77;
Claude["Scores"]["react"] = 52;
Claude["Scores"]["python"] = 92;
Claude["Scores"]["java"] = 67;
Claude["Sumofpoints"] =
  Claude["Scores"]["javascript"] +
  Claude["Scores"]["react"] +
  Claude["Scores"]["python"] +
  Claude["Scores"]["java"];
Claude["Averageofpoints"] = Claude["Sumofpoints"] / 4;
Claude["GPA"] =
  (credits["javascript"] * 2 +
    credits["react"] * 0.5 +
    credits["python"] * 4 +
    credits["java"] * 1) /
  credits["sum"];

// Vincent van Gogh
Van["FirstName"] = "Van";
Van["LastName"] = "Gogh";
Van["Age"] = 21;
Van["Scores"] = [];
Van["Scores"]["javascript"] = 51;
Van["Scores"]["react"] = 98;
Van["Scores"]["python"] = 65;
Van["Scores"]["java"] = 70;
Van["Sumofpoints"] =
  Van["Scores"]["javascript"] +
  Van["Scores"]["react"] +
  Van["Scores"]["python"] +
  Van["Scores"]["java"];
Van["Averageofpoints"] = Van["Sumofpoints"] / 4;
Van["GPA"] =
  (credits["javascript"] * 0.5 +
    credits["react"] * 4 +
    credits["python"] * 1 +
    credits["java"] * 1) /
  credits["sum"];

// Damme Square
Damme["FirstName"] = "Damme";
Damme["LastName"] = "Square";
Damme["Age"] = 36;
Damme["Scores"] = [];
Damme["Scores"]["javascript"] = 82;
Damme["Scores"]["react"] = 53;
Damme["Scores"]["python"] = 80;
Damme["Scores"]["java"] = 65;
Damme["Sumofpoints"] =
  Damme["Scores"]["javascript"] +
  Damme["Scores"]["react"] +
  Damme["Scores"]["python"] +
  Damme["Scores"]["java"];
Damme["Averageofpoints"] = Damme["Sumofpoints"] / 4;
Damme["GPA"] =
  (credits["javascript"] * 3 +
    credits["react"] * 0.5 +
    credits["python"] * 2 +
    credits["java"] * 1) /
  credits["sum"];

//  AVG
let studentsAverage =
  (Damme["Averageofpoints"] +
    Van["Averageofpoints"] +
    Claude["Averageofpoints"] +
    Jean["Averageofpoints"]) /
  4;

// Status
Damme["Status"] = Damme["Averageofpoints"]  >= studentsAverage ?  "WITELI DIPLOMI!" : "VRAG NARODA!";
Van["Status"] = Van["Averageofpoints"]  >= studentsAverage ?  "WITELI DIPLOMI!" : "VRAG NARODA!";
Claude["Status"] = Claude["Averageofpoints"]  >= studentsAverage ?  "WITELI DIPLOMI!" : "VRAG NARODA!";
Jean["Status"] = Jean["Averageofpoints"]  >= studentsAverage ?  "WITELI DIPLOMI!" : "VRAG NARODA!";
// printInfo

console.log("Students: ", Jean, Claude, Van, Damme);

// GPA Sort
let leaderBoard=[];
leaderBoard['I'];
leaderBoard['II'];
leaderBoard['III'];
leaderBoard['IV'];

// Comparison 1
if(Jean["GPA"] > Claude["GPA"]){
    leaderBoard['I']=Jean;
    leaderBoard['II']=Claude;
}else{
    leaderBoard['I']=Claude;
    leaderBoard['II']=Jean;
}
// Comparison 2
if(Van["GPA"] > Damme["GPA"]){
    leaderBoard['III']=Van;
    leaderBoard['IV']=Damme;
}else{
    leaderBoard['III']=Damme;
    leaderBoard['IV']=Van;
}
// Final comparison
if(leaderBoard["III"]["GPA"] > leaderBoard["II"]["GPA"]){
    let tmp= leaderBoard["II"];
    leaderBoard["II"]=leaderBoard["III"];
    leaderBoard["III"]=tmp;
}
if(leaderBoard["IV"]["GPA"] > leaderBoard["III"]["GPA"]){
  let tmp= leaderBoard["III"];
  leaderBoard["III"]=leaderBoard["IV"];
  leaderBoard["IV"]=tmp;
}
if(leaderBoard["II"]["GPA"] > leaderBoard["I"]["GPA"]){
    let tmp= leaderBoard["I"];
    leaderBoard["I"]=leaderBoard["II"];
    leaderBoard["II"]=tmp;
}
console.log("GPA Leader Board", leaderBoard);


// age and average Leader
if((Jean["Averageofpoints"] > Claude["Averageofpoints"] && Jean["Averageofpoints"] > Van["Averageofpoints"] && Jean["Averageofpoints"] > Damme["Averageofpoints"]) && Jean["Age"]>21){
    console.log("Best avg over 21", Jean);
}
if((Claude["Averageofpoints"] > Jean["Averageofpoints"] && Claude["Averageofpoints"] > Van["Averageofpoints"] && Claude["Averageofpoints"] > Damme["Averageofpoints"]) && Claude["Age"]>21){
    console.log("Best avg over 21", Claude);
}
if((Van["Averageofpoints"] > Claude["Averageofpoints"] && Van["Averageofpoints"] > Jean["Averageofpoints"] && Van["Averageofpoints"] > Damme["Averageofpoints"]) && Van["Age"]>21){
    console.log("Best avg over 21", Van);
}
if((Damme["Averageofpoints"] > Claude["Averageofpoints"] && Damme["Averageofpoints"] > Van["Averageofpoints"] && Damme["Averageofpoints"] > Jean["Averageofpoints"]) && Damme["Age"]>21){
    console.log("Best avg over 21", Damme);
}


// Front-end Leader
let jeanFrontavg=(Jean["Scores"]["javascript"] + Jean["Scores"]["react"])/2;
let claudeFrontavg=(Claude["Scores"]["javascript"]+ Claude["Scores"]["react"])/2;
let vanFrontavg=(Van["Scores"]["javascript"]+Van["Scores"]["react"])/2;
let dammeFrontavg=(Damme["Scores"]["javascript"]+Damme["Scores"]["react"])/2;

let highFrontendAVG=jeanFrontavg;
let highestFrontendStudent=Jean;

if(highFrontendAVG < claudeFrontavg){
  highFrontendAVG=claudeFrontavg;
  highestFrontendStudent=Claude;
}
if(highFrontendAVG < vanFrontavg){
  highFrontendAVG=vanFrontavg;
  highestFrontendStudent=Van;
}
if(highFrontendAVG < dammeFrontavg){
  highFrontendAVG=dammeFrontavg;
  highestFrontendStudent=Damme;
}
console.log(highestFrontendStudent);
